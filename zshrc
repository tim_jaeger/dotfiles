# -*- mode: shell-script -*-
source ~/dotfiles/zgen/zgen.zsh

if ! zgen saved; then
    echo "Creating a zgen save"

    zgen oh-my-zsh

    # plugins
    zgen oh-my-zsh plugins/aws
    zgen oh-my-zsh plugins/cabal
    zgen oh-my-zsh plugins/command-not-found
    zgen oh-my-zsh plugins/common-aliases
    # zgen oh-my-zsh plugins/emacs
    zgen oh-my-zsh plugins/git
    zgen oh-my-zsh plugins/lein
    zgen load spwhitt/nix-zsh-completions
    zgen oh-my-zsh plugins/sudo
    zgen oh-my-zsh plugins/systemd
    zgen oh-my-zsh plugins/taskwarrior
    zgen oh-my-zsh plugins/tmuxinator
    zgen oh-my-zsh plugins/z
    zgen load zsh-users/zsh-syntax-highlighting
    # zgen load djui/alias-tips
    # zgen load mfaerevaag/wd
    # zgen load peterhurford/git-aliases.zsh
    zgen load voronkovich/gitignore.plugin.zsh
    zgen load rimraf/k

    # completions
    zgen load zsh-users/zsh-completions src

    # theme
    zgen oh-my-zsh themes/lambda

    # save all to init script
    zgen save
fi


export EDITOR="emacsclient -t"
export BROWSER=chromium
export NIXPKGS=~/.nixpkgs

alias pingg="ping -c 5 8.8.8.8"
alias mtrr="sudo mtr 8.8.8.8"
alias vi="vim"
alias emd="emacs --daemon"
alias e="emacsclient -n"
alias emc="emacsclient -nc"
alias emt="emacsclient -t"
alias ix="curl -F 'f:1=<-' ix.io"
alias kbd="asus-kdb-backlight"
alias n="ncmpcpp"
alias nrs="sudo nixos-rebuild switch --upgrade"
alias rm="rm -rf"

cabconf() {
    cabal2nix --shell ./. > shell.nix &&
    PKGDB=$(nix-shell --command "ghc-pkg list | head -n 1 | sed 's/:$//'") &&
    cabal configure --package-db=$PKGDB
}

open() {
    # xdg-open $@ &
    zathura $@ &>/dev/null &
}

transfer() { if [ $# -eq 0 ]; then echo "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi
             tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; }; alias transfer=transfer

[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ '
