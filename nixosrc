# -*- mode: nix -*-
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot = {
    loader = {
    gummiboot.enable = true;
    efi.canTouchEfiVariables = true;
    };

    kernelPackages = pkgs.linuxPackages_4_3;
    kernelParams = [ "acpi_osi=" ];
  };

  nix.binaryCaches = [
    # "http://hydra.cryp.to/"
    # "http://hydra.nixos.org/"
    "https://cache.nixos.org/"
     ];

  nix.trustedUsers = [ "root" "@wheel" "tjger" ];

  nix.nixPath = [ "/home/tjger/work/hacking" "nixos-config=/home/tjger/dotfiles/nixosrc" ];

  networking = {
    hostName = "zbk"; # Define your hostname.
    domain = "local.domain";
    search = [ "local.domain" ];
    # extraHosts = ''
    #   192.168.1.21 bs
    # '';
    firewall.enable = false;
    # networking.hostId = "e1972e63";
    # networking.wireless.enable = true;  # Enables wireless.
    networkmanager.enable = true;
    networkmanager.packages = with pkgs; [ networkmanagerapplet networkmanager_openvpn networkmanager_openconnect ];
    # nameservers = [
    #   "8.8.8.8"
    #   # "192.168.1.1"
    #   ];
  };

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    stdenv fcron
    nix nix-repl nixops manpages pciutils lm_sensors
    binutils unison htop atop iotop powertop psmisc ncdu lsof file pv cv dstat sysstat unzip dtrx
    zsh tmux tmuxinator termite tree graphviz ack figlet mosh entr
    vim
    emacs gnuplot
    mu isync msmtp
    gitFull gitAndTools.tig gitAndTools.git-crypt gitAndTools.git-extras gitAndTools.gitflow gitAndTools.hub gist darcs
    silver-searcher ranger parallel cloc multitail gnumake
    aspell aspellDicts.en aspellDicts.de units
    wget chromium lynx w3m
    wireshark-cli wireshark-gtk aircrackng # siege
    ngrep mtr netcat-openbsd nmap curl lftp filezilla iftop iptraf nethogs bind wakelan
    rtorrent transmission transmission_gtk
    hexchat weechat newsbeuter
    feh xscreensaver xclip xlibs.xmessage parcellite arandr
    calibre
    # zathura poppler mupdf
    # zathuraCollection.zathura_core zathuraCollection.zathura_pdf_mupdf zathuraCollection.zathura_pdf_poppler zathuraCollection.zathura_ps zathuraCollection.zathura_djvu
    zathuraCollection.zathuraWrapper
    xfce.thunar xfce.thunar_volman
    freetype
    apg # password generator

    gnutls openssl cacert perlPackages.MozillaCA duplicity lastpass-cli gnupg gpgme
    networkmanager networkmanagerapplet networkmanager_openvpn networkmanager_openconnect
    vlc mplayer mpg123
    lxappearance gnome3.gtk gtk-engine-murrine gnome3.gnome_themes_standard tango-icon-theme
    dmenu trayer

    pulseaudioFull pavucontrol paprefs jack2Full
    bluez blueman

    graphviz

    gcc cmake avrdude automake autoconf zlib libffi gstreamer llvm
    avrgcclibc ncurses

    rustc cargo racerRust

    php

    clojure leiningen boot
    # pltScheme

    # pypy
    python27Full python27Packages.virtualenv
    python3 python34Packages.pyyaml python34Packages.glances python34Packages.ipython python34Packages.rainbowstream python34Packages.pgcli
    mopidy mopidy-moped ncmpcpp mopidy-gmusic
    cmus
    beets

    # julia

    gforth
    # elmPackages.elm
    # haskellPackages.idris
    haskellPackages.purescript nodejs nodePackages.bower nodePackages.gulp

    redis hiredis
    haskell.compiler.ghc7103 haskellPackages.happy haskellPackages.alex
    haskellPackages.hoogle
    haskellPackages.cabal-install haskellPackages.cabal2nix haskellPackages.ghci-ng
    haskellPackages.xmonad haskellPackages.xmonad-contrib haskellPackages.xmonad-extras
    haskellPackages.xmobar haskellPackages.yeganesh
    haskellPackages.pandoc haskellPackages.hledger ledger
    haskellPackages.hindent haskellPackages.hlint haskellPackages.stylish-haskell haskellPackages.hasktags haskellPackages.ghc-mod

    lean

    anonymousPro
    eb-garamond
    fira fira-code fira-mono
    hasklig inconsolata iosevka meslo-lg source-code-pro tipa dina-font
    lmodern lmmath
    league-of-moveable-type

    ghostscript
    # texlive.combined.scheme-medium
    (texlive.combine {inherit (texlive) scheme-medium cm-super;})
  ];

  time.timeZone = "Europe/Berlin";

  security = {
    sudo.enable = true;
    sudo.wheelNeedsPassword = false;
    polkit.enable = true;
    rtkit.enable = true;
    rngd.enable = true;
    setuidOwners = [
       { program = "dumpcap";
         owner = "root";
         group = "wireshark";
         setuid = true;
         setgid = false;
         permissions = "u+rx,g+x";
         }
    ];
  };

  # virtualisation.virtualbox.host.enable = true;
  # virtualisation.virtualbox.host.addNetworkInterface = true;

  services = {
    mopidy.enable = true;
    mopidy.extensionPackages = [ pkgs.mopidy-moped pkgs.mopidy-gmusic ];
    mopidy.configuration = ''
      [mpd]
      enabled = true
      hostname = 127.0.0.1
      port = 6600
      password =
      max_connections = 20
      connection_timeout = 60
      zeroconf = Mopidy MPD server on $hostname

      [gmusic]
      username = jger.tm
      password = nzfuabhwaaltrtxe
      bitrate = 320
      # deviceid = 3BCA72B05CA27ED2
      deviceid = 3bca72b05ca27ed2

      [softwaremixer]
      enabled = true

      [http]
      enabled = true
      hostname = 127.0.0.1
      port = 6680
      static_dir =
      zeroconf = Mopidy HTTP server on $hostname
    '';

    ntp.enable = true;
    avahi.enable = true;
    haveged.enable = true;
    # ntopng.enable = true;
    printing.enable = true;
    # printing.drivers = [ pkgs.gutenprint ];
    printing.drivers = [ pkgs.samsungUnifiedLinuxDriver ];
    locate.enable = true;
    locate.period = "00 12 * * *";
    locate.localuser = "root";
    openssh.enable = true;
    rsyncd.enable = true;
    # thermald.enable = true;
    redis.enable = true;
    postgresql.enable = true;
    postgresql.package = pkgs.postgresql95;
    postgresql.authentication = "local all all ident";
    # statsd.enable = true;
    # syncthing.enable = true;
    # syncthing.dataDir = "/home/tjger/.syncthing";
    # syncthing.user = "tjger";
    dbus = {
      enable = true;
      packages = with pkgs; [ networkmanager networkmanagerapplet bluez blueman pulseaudioFull jack2Full xfce.thunar xfce.thunar_volman ];
    };

    gnome3.gnome-keyring.enable = true;
    upower.enable = true;
    udisks2.enable = true;
    # uptimed.enable = true;
    acpid.enable = true;
    # xfs.enable = true;
    xserver = {
        videoDrivers = [ "intel" "i915" ];
        # videoDrivers = [ "intel-testing" ];
        driSupport = true;
        resolutions = [{x = 1920; y = 1080;}];
        defaultDepth = 24;
        enable = true;
        layout = "us";
        xkbVariant = "altgr-intl";
        xkbOptions = "caps:escape";
        synaptics = {
            enable = true;
            twoFingerScroll = true;
            palmDetect = true;
            minSpeed = "0.7";
            maxSpeed = "3";
            accelFactor = "0.05";
        };

        windowManager = {
            xmonad = {
              enable = true;
              enableContribAndExtras = true;
              };
            default = "xmonad";
        };

        desktopManager.default = "none";
        displayManager.slim = {
          defaultUser = "tjger";
          theme = pkgs.fetchurl {
            url = "http://gnome-look.org/CONTENT/content-files/168608-micro_slim_theme.tar.gz";
            sha256 = "96702e573cb0fda6f03fa7e161dacd80e105879b6bbe9018c3ecc367f079b847"; };
        };
        displayManager.sessionCommands = ''
            xsetroot -cursor_name left_ptr &
            nm-applet &
            feh --randomize --bg-fill /home/tjger/dotfiles/wallpapers/ &
            trayer --edge top --align right --monitor primary --widthtype percent --width 5 --height 19 --transparent true --alpha 0 --tint 0x000000 --expand true --SetDockType true --SetPartialStrut true &
            xscreensaver &
            mux start monitoring &
            mux start main &
        '';
    };
  };

  systemd.services.xscreensaver = {
    enable = true;
    before = [ "sleep.target" ];
    description = "Lock X session on sleep using xscreensaver.";
    environment = { DISPLAY = ":0"; };
    path = [ pkgs.xscreensaver ];
    script = "xscreensaver-command --lock";
    # scriptArgs = "--version";
    serviceConfig = { User = "tjger"; Type = "oneshot"; };
    wantedBy = [ "sleep.target" ];
  };

  hardware.bluetooth.enable = true;
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };
  # hardware.pulseaudio.configFile = "/home/tjger/dotfiles/pulserc";

  sound.enable = true;

  powerManagement.enable = true;
  # powerManagement.cpuFreqGovernor = "powersave";
  powerManagement.cpuFreqGovernor = "performance";

  programs.light.enable = true;
  programs.zsh.enable = true;

  # Virtualbox specifics
  # services.virtualboxGuest.enable = true;

  fonts = {
      enableFontDir = true;
      enableGhostscriptFonts = true;
      fontconfig.enable = true;
      fontconfig.includeUserConf = true;
      fontconfig.dpi = 110;
      fontconfig.hinting.style = "slight";
      fontconfig.defaultFonts = {
        monospace = [ "Fira Mono" ];
        sansSerif = [ "Fira Sans" ];
        serif = [ "EB Garamond"];
      };
      fonts = with pkgs; [
        anonymousPro
        dina-font
        eb-garamond
        fira
        fira-code
        fira-mono
        hasklig
        inconsolata
        iosevka
        league-of-moveable-type
        lmodern lmmath
        meslo-lg
        source-code-pro
        tipa
      ];
  };

  users = {
    defaultUserShell = "/run/current-system/sw/bin/zsh";
    # Define a user account. Don't forget to set a password with ‘passwd’.
    extraUsers.tjger = {
        createHome = true;
        home = "/home/tjger";
        description = "Tim Jaeger";
        extraGroups = [ "wheel" "audio" "vboxusers" "networkmanager" ];
        useDefaultShell = true;
        uid = 1000;
        };
    extraGroups.wireshark.gid = 500;
    };

  nix.requireSignedBinaryCaches = false;
  nixpkgs.config = {
    allowBroken = true;
    allowUnfree = true;
    # chromium = {
    #   enablePepperFlash = true;
    #   enablePepperPDF = true;
    # };
    packageOverrides = pkgs: {
      bluez = pkgs.bluez5;
    };
  };

  # nixosManual.showManual = true;
}
