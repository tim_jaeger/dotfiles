" Initialization {{{
" Be iMproved {{{
if &compatible
    set nocompatible
endif
"}}}
" Neovim specifics {{{
if has('nvim')
    runtime! plugin/python_setup.vim
    " set unnamedclip
endif
"}}}
" Installation check {{{
if !has('vim_starting')
    NeoBundleCheck
endif
"}}}
" Automatically reload vimrc {{{
if has("autocmd")
    autocmd bufwritepost .vimrc source %
endif
"}}}
set runtimepath+=~/dotfiles/vim/bundle/neobundle.vim/
"}}}
" Plugins {{{
" Installation "{{{
call neobundle#begin(expand('~/dotfiles/vim/bundle/'))
" NeoBundle setup {{{
NeoBundleFetch 'Shougo/neobundle.vim'
NeoBundleCheck
" }}}
" Libraries {{{
NeoBundle 'Shougo/vimproc.vim', {'build' : {'windows' : 'tools\\update-dll-mingw', 'cygwin' : 'make -f make_cygwin.mak', 'mac' : 'make -f make_mac.mak', 'linux' : 'make', 'unix' : 'gmake',},}
NeoBundle 'xolox/vim-misc'
"}}}
" Editing plugins {{{
NeoBundle 'Shougo/neocomplcache' " Completions with cache
NeoBundle 'Shougo/neosnippet' " Snippeting engine
NeoBundle 'Shougo/neosnippet-snippets' " Default snippet collection
" NeoBundle 'saep/nerdcommenter'
NeoBundle 'tpope/vim-commentary' " Comment out lines
NeoBundle 'sjl/gundo.vim' " Visual undo tree
NeoBundle 'xolox/vim-session' " Session management
NeoBundle 'tpope/vim-characterize' " Get char representation with ga
NeoBundle 'tpope/vim-eunuch' " Provides UNIX-specific commands, primarily :SudoWrite
NeoBundle 'tpope/vim-surround' " Makes surrounding blocks of text easy
NeoBundle 'tpope/vim-fugitive' " Makes surrounding blocks of text easy
NeoBundle 'gregsexton/gitv'
NeoBundle 'Lokaltog/vim-easymotion' " Jump around the file quickly. Use <L><L>w/W
NeoBundle 'chrisbra/NrrwRgn' " Narrow down into a region of file with :NR
" NeoBundle 'godlygeek/tabular'
NeoBundle 'junegunn/vim-easy-align'
NeoBundle 'KabbAmine/zeavim.vim'
"}}}
" UI enhancements {{{
NeoBundle 'tpope/vim-repeat'
NeoBundle 'bling/vim-airline'
NeoBundle 'mkitt/tabline.vim'
NeoBundle 'mhinz/vim-startify'
NeoBundle 'kien/ctrlp.vim'
NeoBundle 'JazzCore/ctrlp-cmatcher'
NeoBundle 'rking/ag.vim'
NeoBundle 'airblade/vim-gitgutter'
NeoBundle 'majutsushi/tagbar'
NeoBundle 'junegunn/goyo.vim'
NeoBundle 'junegunn/limelight.vim'
NeoBundle 'ryanss/vim-hackernews'
NeoBundle 'kien/rainbow_parentheses.vim'
"}}}
"Colorschemes {{{
NeoBundle 'whatyouhide/vim-gotham'
NeoBundle 'morhetz/gruvbox'
NeoBundle 'w0ng/vim-hybrid'
NeoBundle 'gregsexton/Muon'
NeoBundle 'gregsexton/Atom'
"}}}
" Language plugins {{{
NeoBundle 'scrooloose/syntastic'
NeoBundle 'sheerun/vim-polyglot'
NeoBundle 'thinca/vim-ref'
NeoBundle 'tpope/vim-leiningen'
NeoBundle 'tpope/vim-projectionist'
NeoBundle 'tpope/vim-dispatch'
NeoBundle 'tpope/vim-fireplace'
NeoBundle 'guns/vim-clojure-static'
NeoBundle 'guns/vim-clojure-highlight'
NeoBundle 'vim-scripts/paredit.vim'
" NeoBundle 'kovisoft/slimv'
NeoBundle 'amontimur/nix.vim'

" Pandoc plugins {{{
NeoBundle 'vim-pandoc/vim-pandoc' 
" NeoBundle 'plasticboy/vim-markdown'
NeoBundle 'vim-pandoc/vim-pandoc-syntax' 
"}}}
" LaTeX plugins {{{
NeoBundle 'LaTeX-Box-Team/LaTeX-Box'
"}}}
" Python plugins {{{
"NeoBundle 'klen/python-mode'
"NeoBundle 'davidhalter/jedi-vim'
"}}}
" Haskell plugins {{{
NeoBundle 'lukerandall/haskellmode-vim'
NeoBundle 'eagletmt/neco-ghc'
NeoBundle 'eagletmt/ghcmod-vim'
NeoBundle 'dag/vim2hs'
" NeoBundle 'nbouscal/vim-stylish-haskell'
"}}}
" Prolog plugins {{{
NeoBundle 'adimit/prolog.vim'
"}}}
" Hardware plugins {{{
NeoBundle 'jplaut/vim-arduino-ino'
"}}}
"}}}
call neobundle#end()
"}}}
" Configuration "{{{
" Fugitive {{{
nnoremap gs :Gstatus<CR>
" }}}
" Neocomplcache {{{
let g:acp_enableAtStartup = 0
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_enable_smart_case = 1
let g:neocomplcache_min_syntax_length = 3
let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'
let g:neocomplcache_enable_camel_case_completion = 1
let g:neocomplcache_enable_underbar_completion = 1
inoremap <expr><C-g>     neocomplcache#undo_completion()
inoremap <expr><TAB>     neocomplcache#complete_common_string()
"inoremap <expr><TAB>      pumvisible() ? "\<C-n>" : "\<TAB>"
""set completeopt+=longest
"}}}
" Neosnippet {{{
" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif
"}}}
" Rainbow_parentheses "{{{
let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0

ab VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces
"}}}
" Gundo {{{
nnoremap <F5> :GundoToggle<CR>
"}}}
" Vim-session {{{
let g:session_default_overwrite = 0
let g:session_autoload = 0
let g:session_extension = ""
let g:session_autosave = 'yes'
"}}}
" Bufferline {{{
let g:bufferline_echo = 1
"}}}
" CtrlP {{{
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_show_hidden = 1
" let g:ctrlp_user_command = "pt -S --nocolor -g '\\*' %s"
let g:ctrlp_user_command = 'ag %s -Sl --nocolor -g ""'
"}}}
" Ag.vim {{{
let g:agprg="pt --column"
"}}}
" Haskellmode {{{
" TODO: Fix haddock lookup
let g:haddock_browser = "/usr/bin/chromium"
let g:ghc = "/usr/bin/ghc"
let g:haddock_indexfiledir = "~/dotfiles/vim/"
let g:haskellmode_completion_ghc = 0
let g:haskellmode_completion_haddock = 0
"}}}
" Pymode {{{
" TODO: Fix immediate insertion of first completion option when using method
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'
let g:pymode_lint = 1
let g:pymode_lint_checker = "pyflakes,pep8"
let g:pymode_lint_write = 1
let g:pymode_virtualenv = 1
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
let g:pymode_folding = 0
"}}}
" Pandoc {{{
let g:pandoc#syntax#conceal#use = 1
let g:pandoc#folding#mode = "syntax"
" let g:pandoc#command#autoexec_command = "Pandoc pdf"
" let g:pandoc#command#autoexec_on_writes = 1
" let g:pandoc#filetypes#pandoc_markdown = 0
"}}}
" NERDCommenter {{{
" TODO: Doesnt use alt comment style
let g:NERD_haskell_alt_style = 1
"}}}
" Tagbar {{{
let g:tagbar_type_haskell = {
            \ 'ctagsbin' : 'lushtags',
            \ 'ctagsargs' : '--ignore-parse-error --',
            \ 'kinds' : [
            \ 'm:module:0',
            \ 'e:exports:1',
            \ 'i:imports:1',
            \ 't:declarations:0',
            \ 'd:declarations:1',
            \ 'n:declarations:1',
            \ 'f:functions:0',
            \ 'c:constructors:0'
            \ ],
            \ 'sro' : '.',
            \ 'kind2scope' : {
            \ 'd' : 'data',
            \ 'n' : 'newtype',
            \ 'c' : 'constructor',
            \ 't' : 'type'
            \ },
            \ 'scope2kind' : {
            \ 'data' : 'd',
            \ 'newtype' : 'n',
            \ 'constructor' : 'c',
            \ 'type' : 't'
            \ }
            \ }
"}}}
" Airline {{{
set laststatus=2
let g:airline_powerline_fonts = 1
" let g:airline_theme = 'gotham'
let g:airline#extensions#syntastic#enabled     =  1
let g:airline#extensions#tabline#enabled       =  1
let g:airline#extensions#tabline#tab_nr_type   =  1 " tab number
let g:airline#extensions#tabline#fnamecollapse =  1 " /a/m/model.rb
"}}}
" Startify {{{
let g:startify_custom_header = map(split(system('fortune -o hitchhiker'), '\n'), '"   ". v:val') + ['','']
let g:startify_session_persistence = 1
let g:startify_change_to_dir = 1
let g:startify_session_dir = '~/dotfiles/vim/sessions'
let g:startify_session_delete_buffers = 1
"}}}
" LaTeX-Box {{{
let g:tex_flavor = "latex"
let g:LatexBox_quickfix = 4
let g:LatexBox_latexmk_options = "-xelatex -shell-escape -silent"
let g:LatexBox_viewer = "zathura"
let g:LatexBox_Folding = 1
let g:LatexBox_fold_envs = 1
"let g:LatexBox_latexmk_async = 1
"let g:LatexBox_latexmk_preview_continously = 1
let g:LatexBox_ignore_warnings
            \ = ['xparse', 'no line here']
"}}}
" Ag {{{
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  " let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  " let g:ctrlp_use_caching = 0
endif
"}}}
" Vim-Ref {{{
let g:ref_open = "vsplit"
"}}}
" Goyo {{{
nnoremap <F6> :Goyo<CR>
autocmd User GoyoEnter Limelight
autocmd User GoyoLeave Limelight!
"}}}
" Vim-easy-align {{{
vmap <Enter> <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
let g:easy_align_delimiters = {
\ '(': {
\     'pattern':      '(',
\     'left_margin':  1,
\     'right_margin': 0,
\     },
\ '{': {
\     'pattern':      '{',
\     'left_margin':  1,
\     'right_margin': 0,
\     },
\ '[': {
\     'pattern':      '[',
\     'left_margin':  1,
\     'right_margin': 0,
\     },
\ '>': {
\     'pattern':      '->\|<-\|=>\|<=\|>\|<',
\     'left_margin':  1,
\     'right_margin': 1,
\     },
\ }
"}}}
"}}}
"}}}
" UI {{{
" Set current number absolute, rest relative
set relativenumber
" Use line numbering
set number
" Set size of number column
set numberwidth=3
" Set size of command line
set cmdheight=2
" Set size of fold column
set foldcolumn=2
" Retain hidden buffers
set hidden
" Set title according to filename
" TODO: Fix?
set title
" Make vim break everything silently
set visualbell
" Show line and column number
set ruler
" Enable soft-wrapping
set wrap
" Wrap lines at sensible position, not just last char
set linebreak
" Maintain indentation with soft-wrapped lines
set breakindent
" Display non-printing chars
set list
set listchars=tab:▸\ ,eol:¬
" Enably syntax highlighting
syntax on
" colorscheme hybrid
colorscheme muon
" gVim specifics
if has('gui_running')
    " set guioptions=c
    set guioptions-=m
    set guioptions-=r
    set guioptions-=T
    set guioptions-=L

    set guiheadroom=0

    set guifont=Droid\ Sans\ Mono\ for\ Powerline\ 12

    colorscheme muon
endif

"}}}
" Editing {{{
set backspace=indent,eol,start
set autoread
set autowrite
set nospell
"}}}
" Search {{{
" Search case insensitive
set ignorecase
" Search case-sensitive when search string contains uppercase char
set smartcase
" Search while typing
set incsearch
" Hightlight all matches
set hlsearch
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
"}}}
" Undo {{{
set undodir=~/dotfiles/vim/tmp/undo//
set undofile
set history=100
set undolevels=100
set noswapfile
"}}}
" Backup {{{
" set backupdir=~/dotfiles/vim/tmp/backup//
" set backupskip=/tmp/*,/private/tmp/*"
" set backup
set nobackup
" set writebackup
"}}}
" Tabulation {{{
" Make a tab char equal to 8 spaces
set tabstop=8
set softtabstop=4
set expandtab
" Depth of indentation
set shiftwidth=4
" Align to multiples of shiftwidth
set shiftround
"}}}
" Keybindings {{{
" Leader keys {{{
let mapleader = "\<Space>"
let maplocalleader = "\<Space>"
"}}}
" Saving & quitting {{{
"nnoremap <Leader>w :w<CR>
nnoremap ZA :xa<CR>
"}}}
" Move through wrapped lines easily {{{
map j gj
map k gk
"}}}
" Move a line of text using Ctrl+[jk] {{{
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
" inoremap <C-j> <Esc>:m .+1<CR>==gi
" inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv
"}}}
nmap <silent> <Leader>s :set spell!<CR>
set pastetoggle=<F2>
" Replace word under cursor {{{
nnoremap <Leader>r :%s/\<<C-r><C-w>\>//g<Left><Left>
"}}}
" NeoSnippets breaks digraph entry, this remaps it to C-d
inoremap <C-d> <C-k>
"}}} 

filetype plugin indent on

" vim:fdm=marker
